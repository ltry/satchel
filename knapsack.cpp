#include "knapsack.h"
#include "ui_knapsack.h"
#include <QPushButton>

#include "handler.h"
#include "evaluator.h"

#include <QDebug>
#include <QTableView>
#include <QSpacerItem>
void knapsack::setStatusBar(){
    this->statusBar()->setStyleSheet("margin-right: 2px");
    this->statusBar()->hide();
    QGridLayout* status = new QGridLayout(nullptr);
    QLabel *total = new QLabel("Total: "),
            *_tcount = new QLabel("count"),
            *_tcost = new QLabel("cost"),
            *_tweight = new QLabel("weight");
    QFont temp = total->font();
    temp.setBold(1);
    temp.setPointSize(8);
    total->setFont(temp);
    _tcount->setFont(temp);
    _tcost->setFont(temp);
    _tweight->setFont(temp);

    scount = new QLabel(QString::number(0));
    scost = new QLabel(QString::number(0));
    sweight = new QLabel(QString::number(0));

    status->addWidget(total, 0, 0, 1, 1, Qt::AlignLeft);
    status->addWidget(_tcount, 0, 1, 1, 1, Qt::AlignLeft);
    status->addWidget(_tcost, 0, 2, 1, 1, Qt::AlignLeft);
    status->addWidget(_tweight, 0, 3, 1, 1, Qt::AlignLeft);
    status->addWidget(scount, 1, 1, 1, 1, Qt::AlignLeft);
    status->addWidget(scost, 1, 2, 1, 1, Qt::AlignLeft);
    status->addWidget(sweight, 1, 3, 1, 1, Qt::AlignLeft);

    QTableView* upper = new QTableView(nullptr);
    upper->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    upper->setMaximumSize(700, 50);
    upper->setLayout(status);
    this->statusBar()->addPermanentWidget(upper);
}
knapsack::knapsack(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::knapsack){

    ui->setupUi(this);
    ui->centralWidget = ui->SArea;
    ui->SArea->setMaximumWidth(700);
    ui->View->hide();

    QFont font = ui->AItem->font();
    font.setPointSize(8);
    ui->AItem->setFont(font);
    ui->RItem->setFont(font);
    ui->Calc->setFont(font);
    ui->View->setFont(font);

    ui->LOut->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    ui->SArea->scrollBarWidgets(Qt::AlignBottom);
    ui->RItem->setMinimumSize(ui->AItem->minimumSize());
    ui->Calc->setMinimumSize(ui->RItem->minimumSize());
    this->setStatusBar();
}

knapsack::~knapsack(){
    delete ui;
}

void knapsack::on_AItem_released(){
    Item* cur = new Item(ui->LOut);
    handler::Handler->addItem(cur);
    ui->LOut->addWidget(cur);
}

#include <QDebug>
void knapsack::on_RItem_released(){
    int mode = handler::Handler->getMode();
    QString text = (mode?"Remove items":"Double-click to remove");
    ui->RItem->setText(text);
    handler::Handler->setMode(mode^1);
}

#include <QTableView>
#include <QMessageBox>
void knapsack::on_Calc_released(){
    if (!handler::Handler->calcMode){
        if (!Item::noMistakes()){
            QMessageBox::warning(this, "Error", "There're mistakes in fields");
            return;
        }
        if (ui->Capacity->styleSheet() == _red_stylesheet || ui->Capacity->toPlainText().size() == 0){
            QMessageBox::warning(this, "Error", "Mistake in capacity field");
            return;
        }else{
            bool ok;
            qlonglong cur = ui->Capacity->toPlainText().toLongLong(&ok);
            if (cur > 1e9){
                QMessageBox::warning(this, "Error", "Capacity is too big");
                return;
            }
            handler::Handler->W = int(cur);
            if (!ok){
                QMessageBox::warning(this, "Error", "Unexpected error occured");
                return;
            }
        }

        if (!handler::Handler->append()) return;
        handler::Handler->calcMode ^= 1;
        ui->View->show();
        ui->View->setChecked(0);
        ui->Calc->setText("Back to editing");
        ui->Capacity->setDisabled(1);
        ui->AItem->setDisabled(1);
        ui->RItem->setDisabled(1);

        QTextCursor cursor = ui->Capacity->textCursor();
        cursor.clearSelection();
        ui->Capacity->setTextCursor(cursor);

        this->scount->setText(QString::number(handler::Handler->total_count));
        this->scost->setText(QString::number(handler::Handler->total_cost));
        this->sweight->setText(QString::number(handler::Handler->total_weight));
        this->ui->SArea->setStyleSheet("margin-bottom: 0px");
        this->statusBar()->show();

        return;
    }
    if (!handler::Handler->hide()) return;
    handler::Handler->calcMode ^= 1;
    ui->View->hide();
    ui->Calc->setText("Calculate");
    ui->Capacity->setDisabled(0);
    ui->AItem->setDisabled(0);
    ui->RItem->setDisabled(0);
    this->statusBar()->hide();
}

void knapsack::on_Capacity_textChanged(){
    bool ok;
    ui->Capacity->toPlainText().toInt(&ok);
    if (!ok){
        ui->Capacity->setStyleSheet(_red_stylesheet);
    }else{
        ui->Capacity->setStyleSheet("");
    }
}

void knapsack::on_View_released(){
    if (!handler::Handler->calcMode) return;
    handler::Handler->changeView(handler::Handler->viewMode^1);
}
