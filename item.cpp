#include "item.h"
#include "handler.h"
#include "forward.h"
#include "ftextedit.h"

#include <QMenu>
#include <QAction>
#include <QDebug>

Item::Item(QObject* _parent)
{    
    parent = _parent;
    sname = new FTextEdit(this);
    this->setLayout(&upper);
    this->setFocusPolicy(Qt::StrongFocus);
    group.setLayout(&contents);

    contents.setAlignment(Qt::AlignLeft);
    upper.addWidget(&group, 0, Qt::AlignTop);

    group.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    this->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    contents.addWidget(&name, 0, 0, 1, 3, Qt::AlignTop | Qt::AlignLeft);
    contents.addWidget(sname, 0, 0, 1, 3, Qt::AlignTop | Qt::AlignLeft);
    contents.addWidget(&maxcount, 2, 0, 1, 1, Qt::AlignTop | Qt::AlignLeft);
    contents.addWidget(&cost, 2, 1, 1, 1, Qt::AlignTop | Qt::AlignLeft);
    contents.addWidget(&weight, 2, 2, 1, 1, Qt::AlignTop | Qt::AlignLeft);

    LCount.setText("Maximum count");
    LCost.setText("Cost per each");
    LWeight.setText("Weight per each");
    contents.addWidget(&LCount, 1, 0);
    contents.addWidget(&LCost, 1, 1);
    contents.addWidget(&LWeight, 1, 2);

    group.setMaximumWidth(700);
    name.setMaximumSize(650, 40);
    cost.setMaximumSize(100, 30);
    weight.setMaximumSize(100, 30);
    maxcount.setMinimumSize(100, 30);
    maxcount.setMaximumSize(100, 30);

    cost.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    weight.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QFont font;
    font.setPointSize(20);
    name.setFont(font);
    font.setPixelSize(18);
    cost.setFont(font);
    weight.setFont(font);
    maxcount.setFont(font);
    maxcount.setMaximum(200);


    connect(&cost, SIGNAL(textChanged()), this, SLOT(ValChanged()));
    connect(&weight, SIGNAL(textChanged()), this, SLOT(ValChanged()));
    connect(&name, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ContexMenu(const QPoint&)));

    sname->hide();
    sname->setFont(name.font());
    sname->setMinimumSize(name.minimumSize());
    sname->setMaximumSize(name.maximumSize());
    sname->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    name.setContextMenuPolicy(Qt::CustomContextMenu);
}
Item::~Item(){
    Item::mistakes -= state;
}

qint16 Item::mistakes = 0;

void Item::setName(const QString& _name){
    name.setText(_name);
}
void Item::showName(){
    name.show();
}
void Item::mouseDoubleClickEvent(QMouseEvent*){
    if (handler::Handler->getMode()){
        handler::Handler->remItem(this);
        return;
    }
    EditClicked();
}
QObject* Item::getParent(){
    return parent;
}

int Item::getMaxCount(){
    return maxcount.value();
}
QString Item::getCost(){
    return cost.toPlainText();
}
QString Item::getWeight(){
    return weight.toPlainText();
}

bool Item::noMistakes(){
    return mistakes == 0;
}

void Item::ValChanged(){
    QTextEdit* cur = static_cast<QTextEdit*>(sender());

    bool ok;
    qlonglong val = cur->toPlainText().toLongLong(&ok);

    bool prev = cur->styleSheet().indexOf("red") == -1;
    if ((ok && val > 0) || (cur->toPlainText().size() == 0)){
        cur->setStyleSheet("");
    }else cur->setStyleSheet(_red_stylesheet);
    bool next = cur->styleSheet().indexOf("red") == -1;

    qint8 diff = qint8(next)-qint8(prev);
    mistakes += diff; state += diff;
}
void Item::ContexMenu(const QPoint& pos){
    QMenu menu;
    QAction edit("Edit", this);
    connect(&edit, SIGNAL(triggered()), this, SLOT(EditClicked()));
    menu.addAction(&edit);
    menu.exec(mapToGlobal(QPoint(pos.x()+20, pos.y()+20)));
}
void Item::EditClicked(){
    sname->setText(name.text());
    sname->show();
    name.hide();
    name.clear();

    sname->setFocus();
}
