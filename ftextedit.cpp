#ifndef HANDLER_H
#include "handler.h"
#define HANDLER_H
#endif

#include <QKeyEvent>
#include <QtDebug>
void FTextEdit::keyPressEvent(QKeyEvent* event){
    if (event->key() == Qt::Key_Return
            || event->key() == Qt::Key_Enter){
        focusOutEvent(nullptr);
        return;
    }
    QTextEdit::keyPressEvent(event);
}

void FTextEdit::focusOutEvent(QFocusEvent*){
    if (!this->isVisible()) return;
    parent->setName(this->toPlainText());
    this->hide();
    this->clear();
    parent->showName();
}
