#ifndef ITEM_H
#define ITEM_H
#include "forward.h"

#include <QLabel>
#include <QLayout>
#include <QSpinBox>
#include <QGroupBox>
#include <QTextEdit>

class Item : public QWidget
{
    Q_OBJECT
    friend handler;

private:
    QObject* parent;

    QHBoxLayout upper;
    QGroupBox group;
    QGridLayout contents;

    QLabel name;
    QLabel LCount, LCost, LWeight;
    FTextEdit* sname = nullptr;

    QTextEdit cost, weight;
    QSpinBox maxcount;

    qint16 state = 0;
    static qint16 mistakes;
public:
    Item(QObject* _parent = nullptr);
    ~Item();

    void setName(const QString& _name);
    void showName();
    void mouseDoubleClickEvent(QMouseEvent*);
    QObject* getParent();

    int getMaxCount();
    QString getCost();
    QString getWeight();
    static bool noMistakes();
private slots:
    void ValChanged();
    void ContexMenu(const QPoint&);
    void EditClicked();
};

#endif // ITEM_H
