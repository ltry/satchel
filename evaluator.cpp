#include "evaluator.h"

Evaluator* Evaluator::evaluator = new Evaluator();

#include <QDebug>
#include <QBitArray>
#include <QMessageBox>
typedef QPair <qint16, qint16> pair;
bool Evaluator::check(const QVector <item>& cands){
    errorFlag = 0;

    typedef qlonglong ll;
    ll _max = 0;
    ll esteem = 0;
    for (item _new: cands){
        esteem += W*(W/_new.w+1);
        _max = std::max(_max, ll(W/_new.w)+1);
    }

    if (esteem >= 1e9 || _max > 60000){
        QMessageBox::warning(nullptr, "Warning", "Values are too big");
        errorFlag = 1;
        return 0;
    }
    return 1;
}
QVector <pair> Evaluator::result(const QVector <item>& cands){
    typedef qlonglong ll;
    QVector <pair> res;

    if (!check(cands)) return res;

    QVector <ll> f[2];
    f[0] = f[1] = QVector <ll> (W+1, 0);
    QVector <QVector <int> > rec(cands.size()+1, QVector <int> (W+1, 0));

    int i = 0, j = 0;
    for (item _new: cands){
        for (int _j = 0; _j <= W; ++ _j)
            f[(i&1)^1][_j] = 0;
        ll _max = -1;
        for (int prev = 0; prev <= W; ++ prev){
            if (f[i&1][prev] <= _max) continue;
            _max = f[i&1][prev];

            for (int cnt = 0; cnt <= _new.mx && prev+_new.w*cnt <= W; ++ cnt){
                int next = prev+_new.w*cnt;
                ll c = f[i&1][prev]+_new.c*1LL*cnt;
                if (f[(i&1)^1][next] < c){
                    f[(i&1)^1][next] = c;
                    rec[i+1][next] = cnt;
                }
            }
        }
        ++ i;
    }
    for (int _j = 0; _j <= W; ++ _j)
        if (f[i&1][_j] > f[i&1][j])
            j = _j;

    while (i && j){
        if (rec[i][j])
            res.push_back(pair (qint16(i-1), qint16(rec[i][j])));
        if (i){
            j -= cands[i-1].w*rec[i][j];
            -- i;
        }
    }

    return res;
}
