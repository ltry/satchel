App for solving usual knapsack problem.

It allows to add\edit\remove items from list, set their max count, cost per each, weight per each.

After specifying knapsack's capacity you can switch to optimal set of items and then get back to editing.