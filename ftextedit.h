#ifndef FTEXTEDIT_H
#define FTEXTEDIT_H
#include "forward.h"

#include <QTextEdit>
class FTextEdit: public QTextEdit{
    Q_OBJECT
private:
    Item* parent = nullptr;
public:
    FTextEdit();
    FTextEdit(Item* parent): parent(parent){}

    void keyPressEvent(QKeyEvent*);
    void focusOutEvent(QFocusEvent*);
};

#endif // FTEXTEDIT_H
