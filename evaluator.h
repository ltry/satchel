#ifndef EVALUATOR_H
#define EVALUATOR_H

#include <QVector>
class Evaluator{
public:
    struct item{
        int mx, w;
        qlonglong c;

        item(): mx(0), w(0), c(0) {}
        item(int _mx, qlonglong _c, int _w):
            mx(_mx), w(_w), c(_c){}
        bool operator==(const item& other) const{
            return mx == other.mx && c == other.c && w == other.w;
        }
    };
    int W = 0;
    static Evaluator* evaluator;

    QVector <item> last;
    bool check(const QVector <item>&);
    bool errorFlag;
    QVector <QPair <qint16, qint16> > result(const QVector <item>&);
};

#endif // EVALUATOR_H
