#ifndef HANDLER_H
#define HANDLER_H

#include "item.h"
#include "forward.h"
#include "ftextedit.h"
#include "knapsack.h"
#include "evaluator.h"

#include <QVector>
class handler{
    friend knapsack;
private:
    QVector <Item*> items;
    QVector <Evaluator::item> cands;
    QVector <QPair <qint16, qint16> > res;

    bool calcMode = 0,
        viewMode = 0,
        remMode = 0;
    int W;
    qlonglong total_cost = 0;
    int total_count = 0, total_weight = 0;

public:
    static handler* Handler;
    static knapsack* Knapsack;
    handler();
    int getMode();
    void setMode(int);

    int getSize();
    void addItem(Item*);
    void remItem(Item*);

    void setDisabledAll(bool);
    bool append();
    bool hide();
    void changeView(bool);
};


#endif // HANDLER_H
