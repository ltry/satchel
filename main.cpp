#include "handler.h"
#include "knapsack.h"

#include <QApplication>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    knapsack w;
    handler::Knapsack = &w;

    w.show();

    return a.exec();
}
