#ifndef KNAPSACK_H
#define KNAPSACK_H

#include "forward.h"

#include <QLabel>
#include <QMainWindow>
namespace Ui {
class knapsack;
}

class knapsack : public QMainWindow
{
    Q_OBJECT
private:
    QLabel *scount, *scost, *sweight;
    void setStatusBar();
public:
    explicit knapsack(QWidget *parent = nullptr);
    ~knapsack();

    friend handler;
private slots:
    void on_AItem_released();
    void on_RItem_released();
    void on_Calc_released();
    void on_Capacity_textChanged();
    void on_View_released();

private:
    Ui::knapsack *ui;
};

#endif // KNAPSACK_H
