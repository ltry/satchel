#ifndef HANDLER_H
#include "handler.h"
#define HANDLER_H
#endif

#include <QDebug>
handler* handler::Handler = new handler();
knapsack* handler::Knapsack = nullptr;

handler::handler(){
    items.clear();
}
int handler::getMode(){
    return remMode;
}
void handler::setMode(int mode){
    remMode = mode;
}
int handler::getSize(){
    return items.size();
}
void handler::addItem(Item* item){
    item->setName(QString::number(items.size()));
    items.push_back(item);
}
void handler::remItem(Item* item){
    items.remove(items.indexOf(item));
    QLayout* upper = static_cast<QLayout*>(item->getParent());
    upper->removeWidget(item);
    delete item;
}

#include "evaluator.h"
#include "knapsack.h"

#include <QStyle>
#include <QMessageBox>
void handler::setDisabledAll(bool r = 1){
    for (auto i: items){
        if (r){
            QTextCursor cur = i->cost.textCursor();
            cur.clearSelection();
            i->cost.setTextCursor(cur);

            cur = i->weight.textCursor();
            cur.clearSelection();
            i->weight.setTextCursor(cur);
        }

        if (r) i->maxcount.setStyleSheet("border-color: gray");
        else i->maxcount.setStyleSheet("");
        i->setDisabled(r);
    }

}

bool handler::append(){
    setDisabledAll();
    cands.clear();

    for (auto i: items){
        bool ok;
        int maxcount = i->getMaxCount();
        if (!maxcount){
            QMessageBox::warning(nullptr, "Error", "Mistake in max count field");
            setDisabledAll(0);
            return 0;
        }
        qlonglong cost = i->getCost().toInt(&ok);
        if (!ok){
            QMessageBox::warning(nullptr, "Error", "Mistake in cost field");
            setDisabledAll(0);
            return 0;
        }
        qlonglong weight = i->getWeight().toInt(&ok);
        if (!ok){
            QMessageBox::warning(nullptr, "Error", "Mistake in weight field");
            setDisabledAll(0);
            return 0;
        }

        weight = std::min(weight, qlonglong(2.1e9));
        cands.push_back(Evaluator::item(maxcount, cost, int(weight)));
    }

    if (Evaluator::evaluator->last != cands
            || this->W != Evaluator::evaluator->W){
        Evaluator::evaluator->last = cands;
        Evaluator::evaluator->W = this->W;

        res = Evaluator::evaluator->result(cands);
    }
    if (Evaluator::evaluator->errorFlag) return 0;

    for (auto cur: res){
        int i = cur.first, cnt = cur.second;
        items[i]->setStyleSheet("QLabel, QTextEdit, QSpinBox { color: black } QGroupBox { border: 1px solid green }");
        items[i]->LCount.setText("Optimal count");
        items[i]->LCost.setText("Summary cost");
        items[i]->LWeight.setText("Summary weight");

        items[i]->maxcount.setValue(cnt);
        items[i]->cost.setText(QString::number(qlonglong(cnt*1LL*cands[i].c)));
        items[i]->weight.setText(QString::number(cnt*cands[i].w));
    }

    //Status bar

    total_count = total_cost = total_weight = 0;
    for (auto cand: handler::Handler->res){
        int idx = cand.first, cnt = cand.second;
        total_count += cnt;
        total_cost += cnt*1LL*handler::Handler->cands[idx].c;
        total_weight += cnt*handler::Handler->cands[idx].w;
    }

    return 1;
}

bool handler::hide(){
    setDisabledAll(0);
    this->changeView(0);
    for (auto cur: res){
        int i = cur.first;
        items[i]->setStyleSheet("");
        items[i]->LCount.setText("Maximum count");
        items[i]->LCost.setText("Cost per each");
        items[i]->LWeight.setText("Weight per each");

        items[i]->maxcount.setValue(cands[i].mx);
        items[i]->cost.setText(QString::number(cands[i].c));
        items[i]->weight.setText(QString::number(cands[i].w));
    }
    return 1;
}

#include <QBitArray>
void handler::changeView(bool viewincluded = 1){
    if (handler::Handler->calcMode == 0) return;
    viewMode = viewincluded;

    QBitArray included(items.size());
    for (auto i: res) included[i.first] = 1;

    for (int i = 0; i < items.size(); ++ i)
        if (!included[i]){
            if (viewMode) items[i]->hide();
            else items[i]->show();
        }
}
